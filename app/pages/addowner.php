<?php
if (isset($_POST["check_task"]) && isset($_POST["select_user"])) {
    session_start();
    include_once 'app/classes/User.php';
    include_once 'app/config.php';
    $pdo = new PDO($dsn, $user, $pass, $opt);
    $user = new User($_SESSION['user_id'], $pdo);
    $makeDone = $user->SelectOwner($_POST["check_task"], $_POST['select_user']);
} else {
    $_SESSION['error_add_owner'] = " Please select user and task!";
}
header('Location:' . '/distribute');