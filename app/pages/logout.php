<?php
include_once 'app/classes/User.php';
include_once 'app/config.php';
session_start();
$pdo = new PDO($dsn, $user, $pass, $opt);
$user = new User($_SESSION['user_id'], $pdo);
$user->Logout();
header('Location:' . '/index');