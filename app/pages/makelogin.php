<?php
include_once 'app/classes/Auth.php';
include_once 'app/config.php';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['name']) && isset($_POST['password'])) {

        $pdo = new PDO($dsn, $user, $pass, $opt);
        $signUp = new Auth($_POST['name'], $_POST['password'], $pdo);
        $result = $signUp->Login();
        if($result) {
            header('Location:' . '/home');
        }
        else{
            $_SESSION['error_login_wrong'] = "User or password was wrong";
            header('Location:' . '/login');
        }
    }
    else {
        $_SESSION['error_login'] = "User mast have name and password";
    }
}
header('Location:' . '/login');
?>
