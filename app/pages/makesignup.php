<?php
session_start();
include_once 'app/classes/Auth.php';
include_once 'app/config.php';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(isset($_POST['name']) && isset($_POST['password']) && isset($_POST['role_id'])){

        $pdo = new PDO($dsn, $user, $pass, $opt);
        $signUp = new Auth($_POST['name'],$_POST['password'],$pdo);
        $uniq = $signUp->UniqueName();
        if($uniq){
            $result =  $signUp->SignUp($_POST['role_id']);
            header('Location:'.'/home');
            }
        else{
            $_SESSION['error_signup'] = "User with this name exists";
            header('Location:'.'/signup');
        }
    } else {
        $_SESSION['error_signup'] = "User mast have name and password and role";
    }
}
header('Location:'.'/signup');
?>

