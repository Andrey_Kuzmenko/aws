<?php
session_start();
include_once 'app/pages/templates/header.php';
include_once 'app/classes/User.php';
include_once 'app/config.php';
$pdo = new PDO($dsn, $user, $pass, $opt);
$user = new User($_SESSION['user_id'], $pdo);
?>
<body>
<div class="navbar">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">

            <ul class="nav navbar-nav">
                <?php
                echo "<li><h4>Welcome<h4>";
                echo "<h5>" . $user->GetName() . "</h5></li>";
                ?>
                <li class="active"><a href="#">Home</a></li>
                <?php if ($user->GetRole() == 2) {
                    echo ' <li><a href="distribute">Distribute</a></li>';
                }
                ?>
                <li><a href="logout">Logout</a></li>
            </ul>
        </div>
    </nav>
</div>
<div class="container">
    <div class="row">
        <?php if ($user->GetRole() == 1) {
            echo '<form enctype="multipart/form-data" action="uploadtask" method="post">
                <label class="btn btn-default btn-file">
        Browse <input type="file" name="tasks" style="display: none;">
    </label>
     <button type="submit" class="btn btn-primary">Upload</button>
    </form>';
        }
        if (isset($_SESSION['error_upload'])) {
            echo '<div class="alert alert-danger" role="alert">
  <strong>Upps!</strong>' . $_SESSION['error_upload'] . '</div>';
            unset($_SESSION['error_upload']);
        }
        if (isset($_SESSION['upload_success'])) {
            echo '<div class="alert alert-success" role="alert">
  <strong>Well done!</strong> ' . $_SESSION['upload_success'] . '</div>';
            unset($_SESSION['upload_success']);
        }
        ?>
        <h4>Your Task</h4>
        <form action="makedone" method="post">
            <table class="table">
                <thead>
                <tr>
                    <th>Task</th>
                    <th>state</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $taskList = $user->GetTaskList();
                foreach ($taskList as $task) {
                    //  var_dump($task);
                    echo "<tr>";
                    echo "<td>";
                    echo $task['name'];
                    echo "</td>";
                    echo "<td>";
                    if ($task['done'] == 1) {
                        echo '<img src="app/img/done.png" width="20px" height="20px">';
                    } else {
                        echo '<div class="checkbox">
                              <label><input type="checkbox" name="check_done[]" value="' . $task['id'] . '"></label>
                          </div>';
                    }
                    echo "</td>";
                    echo "</tr>";
                }
                ?>
                </tbody>
            </table>
            <?php
            if (isset($_SESSION['check_done_error'])) {
                echo '<div class="alert alert-danger" role="alert">
            <strong>Upps!</strong> ' . $_SESSION['check_done_error'] . '</div>';
                unset($_SESSION['check_done_error']);
            }
            if (isset($_SESSION['done_success'])) {
                echo '<div class="alert alert-success" role="alert">
            <strong>Well done!</strong> ' . $_SESSION['done_success'] . '</div>';
                unset($_SESSION['done_success']);
            }
            if (isset($_SESSION['error_done'])) {
                echo '<div class="alert alert-danger" role="alert">
            <strong>>Upps!</strong> ' . $_SESSION['done_success'] . '</div>';
                unset($_SESSION['error_done']);
            }
            if (isset($_SESSION['distribute_access_error'])) {
                echo '<div class="alert alert-danger" role="alert">
  <strong>Upps!</strong> ' . $_SESSION['distribute_access_error'] . '</div>';
                unset($_SESSION['distribute_access_error']);
            }
            ?>
            <button type="submit" class="btn btn-primary">Update task</button>
        </form>
    </div>
</div>
</body>