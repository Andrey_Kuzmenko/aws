<?php
session_start();
include_once 'app/pages/templates/header.php';
?>
<body>
<div class="container">
    <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-4">

            <form class="form-signin" role="form" action="makesignup" method="post">
                <h2 class="form-signin-heading">Please sign in</h2>
                <input class="form-control" placeholder="Name" required="" autofocus="" name="name">
                <input class="form-control" placeholder="Password" required="" type="password" name="password">
                <div class="checkbox">
                    <label><input type="radio" value="1" name="role_id">Mother</label>
                </div>
                <div class="checkbox">
                    <label><input type="radio" value="2" name="role_id">Father</label>
                </div>
                <div class="checkbox ">
                    <label><input type="radio" value="3" name="role_id">Child</label>
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                <?php
                if (isset($_SESSION['error_signup'])){
                    echo '<p><div class="alert alert-danger">';
                    echo $_SESSION['error_signup'];
                    echo '</div></p>';
                    unset($_SESSION['error_signup']);
                }
                ?>
            </form>
        </div>
    </div>

</div>
</body>