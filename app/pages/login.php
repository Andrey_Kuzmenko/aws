<?php include_once 'app/pages/templates/header.php'; ?>
<body>

<div class="container">
    <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-4">

            <form class="form-signin" role="form" action="makelogin" method="post">
                <h2 class="form-signin-heading">Please Login</h2>
                <input class="form-control" placeholder="Name" required="" autofocus="" name="name">
                <input class="form-control" placeholder="Password" required="" type="password" name="password">
                <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
                <?php
                if (isset($_SESSION['error_login_wrong'])){
                    echo '<p><div class="alert alert-danger">';
                    echo $_SESSION['error_login_wrong'];
                    echo '</div></p>';
                    unset($_SESSION['error_login_wrong']);
                }
                if (isset($_SESSION['error_login'])){
                    echo '<p><div class="alert alert-danger">';
                    echo $_SESSION['error_login'];
                    echo '</div></p>';
                    unset($_SESSION['error_login']);
                }
                ?>
            </form>
        </div>
    </div>

</div>
</body>