<?php
include_once 'app/classes/User.php';
include_once 'app/config.php';
$pdo = new PDO($dsn, $user, $pass, $opt);
$user = new User($_SESSION['user_id'], $pdo);
if ($user->GetRole() != 2) {
    $_SESSION['distribute_access_error'] = 'You don\'t have permissions for page of distribute!';
    header('Location:' . '/home');
}
include_once 'app/pages/templates/header.php';
?>
<body>
<div class="navbar">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">

            <ul class="nav navbar-nav">
                <?php
                echo "<li><h4>Welcome<h4>";
                echo "<h5>" . $user->GetName() . "</h5></li>";
                ?>
                <li><a href="home">Home</a></li>
                <li class="active"><a href="#">Distribute</a></li>
                <li><a href="logout">Logout</a></li>
            </ul>
        </div>
    </nav>
</div>
<div class="container">
    <form action="addowner" method="post">
        <p><h3>Select Owner</h3></p>
    <select class="selectpicker"  name="select_user" data-style="btn-primary">
        <?php
            $userInfo = $user->GetNameList();
            foreach ($userInfo as $item) {
                echo '<option value="' .$item['id'].'">'.$item['name'].'</option>';
        }
        ?>
    </select>
    <div class="row">


        <h4>Task without owner</h4>

            <table class="table">
                <thead>
                <tr>
                    <th>Task</th>
                    <th>state</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $taskList = $user->DistributeList();
                foreach ($taskList as $task) {
                    //  var_dump($task);
                    echo "<tr>";
                    echo "<td>";
                    echo $task['name'];
                    echo "</td>";
                    echo "<td>";
                    echo '<div class="checkbox">
                              <label><input type="checkbox" name="check_task[]" value="' . $task['id'] . '"></label>
                          </div>';
                    echo "</td>";
                    echo "</tr>";
                }
                ?>
                </tbody>
            </table>
        <?php
        if (isset($_SESSION['error_add_owner'])) {
            echo '<div class="alert alert-danger" role="alert">
            <strong>Upps!</strong> ' . $_SESSION['error_add_owner'] . '</div>';
            unset($_SESSION['error_add_owner']);
        }
        if (isset($_SESSION['owner_success'])) {
            echo '<div class="alert alert-success" role="alert">
            <strong>Well done!</strong> ' . $_SESSION['owner_success'] . '</div>';
            unset($_SESSION['owner_success']);
        }
        if (isset($_SESSION['owner_upload_error'])) {
            echo '<div class="alert alert-danger" role="alert">
            <strong>Upps!</strong> ' . $_SESSION['owner_upload_error'] . '</div>';
            unset($_SESSION['owner_upload_error']);
        }
        ?>

            <button type="submit" class="btn btn-primary">Add owner to task(s)</button>
        </form>
    </div>
</div>
</body>