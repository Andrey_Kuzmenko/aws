<?php
if (isset($_POST["check_done"])) {
    session_start();
    include_once 'app/classes/User.php';
    include_once 'app/config.php';
    $pdo = new PDO($dsn, $user, $pass, $opt);
    $user = new User($_SESSION['user_id'], $pdo);
    $makeDone = $user->MakeDone($_POST["check_done"]);
} else {
    $_SESSION['check_done_error'] = "Please select task before update";
}
header('Location:' . '/home');