<?php
session_start();
include_once 'app/classes/User.php';
include_once 'app/config.php';
$pdo = new PDO($dsn, $user, $pass, $opt);
$user = new User($_SESSION['user_id'], $pdo);
if(isset($_FILES["tasks"])) {
    $content = file_get_contents($_FILES["tasks"]["tmp_name"]);
    $content_array = explode("\r\n", $content);
    $user->UploadTasks($content_array);
}
header('Location:' . '/home');