<?php
class Auth
{
    private $user;
    private $password;
    private $pdo;

    /**
     * Auth constructor.
     * @param $user
     * @param $password
     * @param $pdo
     */
    function __construct($user,$password,$pdo) {
        $this->user = $user;
        $this->password = $password;
        $this->pdo = $pdo;
    }

    /**
     * @return bool
     */
    public function UniqueName(){
        $stmt = $this->pdo->query("SELECT name FROM users WHERE name = '".$this->user."'");
        $rows = array();
        foreach($stmt as $row) {
            $rows[] = $row;
        }
        if(count($rows) == 0){
            return true;
        }
        return false;
    }

    /**
     * @param $role
     */
    public function SignUp($role){

        $this->pdo->exec("INSERT INTO `users`(`name`, `password`, `id_role`) VALUES ('".$this->user."','".md5($this->password)."',".$role.")");
        $this->Login();
    }

    /**
     * @return bool
     */
    public function Login(){
        $stmt =  $this->pdo->query("SELECT id,name,id_role FROM users WHERE name = '".$this->user."' AND password = '".md5($this->password)."'");
        $rows = array();
        foreach($stmt as $row) {
            $rows[] = $row;
        }
        if(count($rows) > 0){
            session_start();
            $_SESSION['user_id'] = $rows[0]['id'];
            return true;
        }
        return false;
    }

}