<?php

 class  Redirect
{
    //url list for authentication user
    private static $urlListAuth = ['/home','/uploadtask','/distribute','/makedone','/distribute','/addowner','/logout'];
    //url list for  not authentication user
    private static $urlList = ['/login','/signup','/makelogin','/makesignup'];

     /**
      * @param $url
      */
    static function CheckAuth($url){
        session_start();
        if (isset($_SESSION['user_id'])) {
            if(in_array($url,self::$urlListAuth)){
                include_once ('app/pages'.$url.'.php');
            }
            else{
                include_once ('app/pages/home.php');
            }
        } else {
            if(in_array($url,self::$urlList)){
                include_once ('app/pages'.$url.'.php');
            }
            else{
                include_once ('app/pages/index.php');
            }
        }
    }


}