<?php

class User
{
    private $user_id;
    private $pdo;
    private $role_id;
    private $name;

    /**
     * User constructor.
     * @param $user_id
     * @param $pdo
     */
    function __construct($user_id, $pdo)
    {
        session_start();
        $this->user_id = $user_id;
        $this->pdo = $pdo;
        $stmt = $this->pdo->query("SELECT name,id_role FROM users WHERE id =" . $user_id);
        $rows = array();
        foreach ($stmt as $row) {
            $rows[] = $row;
        }
        if (count($rows) == 0) {
            $this->Logout();
        } else {

            $this->name = $rows[0]['name'];
            $this->role_id = $rows[0]['id_role'];
        }

    }

    /**
     * @return mixed
     */
    public function GetName()
    {
        return $this->name;

    }

    /**
     * @return array
     */
    public function GetNameList()
    {
        if ($this->role_id == 2) {
            $stmt = $this->pdo->query("SELECT id,`name` FROM users ");
            $rows = array();
            foreach ($stmt as $row) {
                $rows[] = $row;
            }
            return $rows;
        }
    }

    /**
     * @return mixed
     */
    public function GetRole()
    {
        return $this->role_id;
    }

    /**
     * @return array
     */
    public function GetTaskList()
    {
        $stmt = $this->pdo->query("SELECT `name`,done,id FROM tasks WHERE user_id =" . $this->user_id);
        $rows = array();
        foreach ($stmt as $row) {
            $rows[] = $row;
        }
        return $rows;
    }

    /**
     * @param $id_task
     */
    public function MakeDone($id_task)
    {
        if (is_array($id_task)) {
            $ids = implode(",", $id_task);
            $this->pdo->exec("UPDATE `tasks` SET `done`= 1 WHERE id IN (" . $ids . ")");
            $_SESSION['done_success'] = 'Task(s) mark(s) updated!';
        } else {

            $_SESSION['error_done'] = 'Task(s) mark(s) not updated!';
        }
    }

    /**
     * @param $tasks_array
     */
    public function UploadTasks($tasks_array)
    {
        if (is_array($tasks_array) && $this->role_id == 1) {
            foreach ($tasks_array as $task) {
                $tasks_array_mod[] = "('" . $task . "')";
            }
            $tasks = implode(",", $tasks_array_mod);
            $this->pdo->exec("INSERT INTO `tasks` (`name`) VALUES " . $tasks);
            $_SESSION['upload_success'] = 'Task(s) add(s) success!';
        } else {
            $_SESSION['error_upload'] = 'Task(s) not add(s)!';
        }
    }

    /**
     * @return array
     */
    public function DistributeList()
    {
        if ($this->role_id == 2) {
            $stmt = $this->pdo->query("SELECT `name`,done,id FROM tasks WHERE user_id  IS NULL");
            $rows = array();
            foreach ($stmt as $row) {
                $rows[] = $row;
            }
            return $rows;
        }
    }

    /**
     * @param $id_tasks
     * @param $id_user
     */
    public function SelectOwner($id_tasks, $id_user)
    {
        if (is_array($id_tasks) && $this->role_id == 2) {
            $ids = implode(",", $id_tasks);
            $this->pdo->exec("UPDATE `tasks` SET `user_id`=" . $id_user . " WHERE id IN (" . $ids . ")");
            $_SESSION['owner_success'] = 'Task(s) now have owner!';
        } else {
            $_SESSION['owner_upload_error'] = 'Task(s) not add(s) owner!';
        }
    }

    public function Logout()
    {
        session_destroy();
    }


}