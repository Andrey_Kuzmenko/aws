-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Авг 20 2017 г., 22:12
-- Версия сервера: 5.5.50
-- Версия PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `aws`
--

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'Mother'),
(2, 'Father'),
(3, 'Child');

-- --------------------------------------------------------

--
-- Структура таблицы `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `done` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tasks`
--

INSERT INTO `tasks` (`id`, `name`, `user_id`, `done`) VALUES
(1, 'Watch TV ', 7, 1),
(2, 'Dusting', 7, 1),
(3, 'Sweeping', 7, 1),
(4, 'Vacuuming', 7, 1),
(13, 'Make something', 7, 1),
(14, 'Good Man', 7, 1),
(15, 'Yachoo', 7, 1),
(16, 'Make something', 7, 1),
(17, 'Good Man', 7, 0),
(18, 'Yachoo', 7, 0),
(19, 'Make something', 8, 1),
(20, 'Good Man', NULL, 0),
(21, 'Yachoo', NULL, 0),
(22, 'Make something', NULL, 0),
(23, 'Good Man', NULL, 0),
(24, 'Yachoo', NULL, 0),
(25, 'Feeding pets', NULL, 0),
(26, 'Doing laundry', NULL, 0),
(27, 'Preparing meals', NULL, 0),
(28, 'Cleaning bathrooms', NULL, 0),
(29, 'Washing bedding', NULL, 0),
(30, 'Mopping floors', NULL, 0),
(31, 'Watering plants', NULL, 0),
(32, 'Mowing the lawn', 8, 1),
(33, 'Weeding the garden', 8, 0),
(34, 'Taking out the trash', 8, 0),
(35, 'Wash the car', 1, 0),
(36, 'Bathing pets', 1, 0),
(37, 'Clean refrigerator', 1, 0),
(38, 'Change air filters on furnace or air conditioner', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `password` varchar(255) NOT NULL,
  `id_role` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `id_role`) VALUES
(1, 'hi', '04e20c7f21f14731418306b0925f7926', 2),
(2, 'hi1', '04e20c7f21f14731418306b0925f7926', 3),
(3, 'Hi im reg', '04e20c7f21f14731418306b0925f7926', 2),
(4, 'New User', '04e20c7f21f14731418306b0925f7926', 1),
(5, 'New User 2', '04e20c7f21f14731418306b0925f7926', 2),
(6, 'New User 21', '987a452b8083e8bcc3b033a7d6982c6b', 2),
(7, 'New User 212', 'a9dfec655ffac1277aebe04ce4294dc5', 1),
(8, 'Father', '04e20c7f21f14731418306b0925f7926', 2),
(9, 'Mother', '04e20c7f21f14731418306b0925f7926', 1),
(10, 'Child', '04e20c7f21f14731418306b0925f7926', 3);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_role` (`id_role`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `tasks_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`id_role`) REFERENCES `roles` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
